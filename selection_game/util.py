import numpy as np
import scipy.stats as st
from scipy.optimize import bisect
from scipy.special import lambertw


def get_mfoc(theta, G, param):
    """
    Calculate the solutions to FOC given theta and group G.
    """
    SC = param.S / param.C
    W1 = np.real(lambertw(-2 * np.pi * param.C[G]**2 / param.S[G]**2 * param.sigma[G]**4, k=0))
    W2 = np.real(lambertw(-2 * np.pi * param.C[G]**2 / param.S[G]**2 * param.sigma[G]**4, k=-1))
    
    x1 = param.sigma[G] * np.sqrt(-W1) # 1st value of (theta - m) at which u''=0
    x2 = param.sigma[G] * np.sqrt(-W2) # 2nd value of (theta - m) at which u''=0
    lower = theta - SC[G] * st.norm(0, param.sigma[G]).pdf(0) # lower bound on (theta - m)
    upper = theta # upper bound on (theta - m)

    # if u' is monotone 
    if SC[G] / param.sigma[G]**2 < 1/st.norm.pdf(1):
        if theta > SC[G] * st.norm(0, param.sigma[G]).pdf(0):
            res = [bisect(lambda x: theta - x - SC[G] * st.norm(0, param.sigma[G]).pdf(x), a=0, b=upper)]
        else:
            res = [bisect(lambda x: theta - x - SC[G] * st.norm(0, param.sigma[G]).pdf(x), a=lower, b=0)] 
    # if u' is not monotone, three cases, depending no value of theta, are possible
    else: 
        # cases when theta is above/below of local max/min of x + S/(C)phi(x,sigma) 
        if theta - x1 > SC[G] * st.norm(0, param.sigma[G]).pdf(x1):
            res = [bisect(lambda x: theta - x - SC[G] * st.norm(0, param.sigma[G]).pdf(x), a=x2, b=upper)]
        elif theta - x2 < SC[G] * st.norm(0, param.sigma[G]).pdf(x2):
            res = [bisect(lambda x: theta - x - SC[G] * st.norm(0, param.sigma[G]).pdf(x), a=lower, b=x1)]
        # case when theta is between the local min and local max of x + S/(C)phi(x,sigma)
        else:
            # find three solutions in [lower, x1], in [x1, x2], and in [x2, upper]
            res1 = bisect(lambda x: theta - x - SC[G] * st.norm(0, param.sigma[G]).pdf(x), a=lower, b=x1)
            res2 = bisect(lambda x: theta - x - SC[G] * st.norm(0, param.sigma[G]).pdf(x), a=x1, b=x2)
            res3 = bisect(lambda x: theta - x - SC[G] * st.norm(0, param.sigma[G]).pdf(x), a=x2, b=upper)
            res = [res1, res2, res3]
    # change of variables from (theta - m) back to m
    return theta - np.array(res)


def get_theta(m, param):
    """Calculate \theta that satisfies budget constraint given the efforts"""
    return bisect(lambda T: param.alpha - np.sum((1 - st.norm(m, param.sigma).cdf(T)) * param.p), a=-10, b=1000)


def get_Tnew(theta, param):
    """Calculate the value of the function T at given \theta"""
    mnew = get_mnew(theta, param)
    return get_theta(mnew, param)


def get_mnew(theta, param):
    """Get best effort given the starting threshold \theta"""
    res0 = get_mfoc(theta, 0, param)
    imax0 = np.argmax(get_payoff(res0, theta, 0, param))
    m0 = res0[imax0]
    
    res1 = get_mfoc(theta, 1, param)
    imax1 = np.argmax(get_payoff(res1, theta, 1, param))
    m1 = res1[imax1]

    mnew = np.array([m0, m1])
    return mnew


def get_Tfp(param):
    """Calculate the fixed point of the function T"""
    Tfp, status = bisect(lambda T: get_Tnew(T, param) - T, a=-100, b=100, full_output=True)
    if not status.converged:
        print("No convergence for {}", param)
    return Tfp


def get_payoff(m, theta, G, param):
    """Calculate individual utility for a G-candidate"""
    return param.S[G] * (1. - st.norm(m, param.sigma[G]).cdf(theta)) - param.C[G] * m**2 / 2


def get_rate(m, theta, G, param):
    """Calculate selection probability (rate) for a G-candidate"""
    return (1. - st.norm(m, param.sigma[G]).cdf(theta))


def get_quality(m, T, G, param):
    """Calculate expected selection quality for group G given the effort and the threshold"""
    rate = get_rate(m, T, G, param)
    return m * rate + param.sigma[G]**2 * st.norm(m, param.sigma[G]).pdf(T)


def get_ne(T, param):
    """Calculate the Nash equilibrium"""
    res0 = get_mfoc(T, 0, param)
    res1 = get_mfoc(T, 1, param)
    
    imax0 = np.argmax(get_payoff(res0, T, 0, param))
    x0 = res0[imax0]
    imax1 = np.argmax(get_payoff(res1, T, 1, param))
    x1 = res1[imax1]

    EPS = 1e-10 
    # check if FOC has two solutions that give (almost) equal payoff for G=0
    if len(res0) > 1 and np.abs(get_payoff(np.min(res0), T, 0, param) - get_payoff(np.max(res0), T, 0, param)) < EPS:
        x0min = np.min(res0)
        x0max = np.max(res0)
        ress0 = np.array([np.min(res0), np.max(res0)])

        rate0min = get_rate(x0min, T, 0, param)
        rate0max = get_rate(x0max, T, 0, param)
        tau0 = (param.alpha - get_rate(x1, T, 1, param)*param.p[1] - rate0max*param.p[0])/(rate0min - rate0max)/param.p[0]
    else:
        ress0 = np.array([x0, x0])
        tau0 = 1
    taus0 = np.array([tau0, 1 - tau0])

    # check if FOC has two solutions that give (almost) equal payoff for G=1
    if len(res1) > 1 and np.abs(get_payoff(np.min(res1), T, 1, param) - get_payoff(np.max(res1), T, 1, param)) < EPS:
        x1min = np.min(res1)
        x1max = np.max(res1)
        ress1 = np.array([np.min(res1), np.max(res1)])

        rate1min = get_rate(x1min, T, 1, param)
        rate1max = get_rate(x1max, T, 1, param)
        tau1 = (param.alpha - get_rate(x0, T, 0, param)*param.p[0]- rate1max*param.p[1]) / (rate1min - rate1max)/param.p[1]
    else:
        ress1 = np.array([x1, x1])
        tau1 = 1
    taus1 = np.array([tau1, 1 - tau1])
    return taus0, ress0, taus1, ress1
