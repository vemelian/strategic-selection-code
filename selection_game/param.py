
class Param(object):
    """Defines the set of parameters of the problem."""
    def __init__(self, alpha=None, C=None, S=None, p=None, sigma=None):
        self.alpha = alpha
        self.C = C
        self.S = S
        self.p = p
        self.sigma = sigma
        
    def copy(self):
        return Param(self.alpha, self.C, self.S, self.p, self.sigma)
